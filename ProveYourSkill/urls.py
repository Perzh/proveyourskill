# coding: utf-8
from django.conf.urls import patterns, include, url
from django.contrib import admin
from main import views
from django.views.generic import RedirectView
from django.conf import settings
from django.conf.urls.static import static

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    # Авторизация и т.п.
    url(r'^login/$', views.oid_begin, name='login'), # begins openid authentication
    url(r'^complete/$', views.oid_complete), # completes openid authentication
    url(r'^logout/$', views.signout, name='logout'),
    # ачивки
    url(r'^achievement/all$', views.achievement_all, name='achievements'),
    url(r'^achievement/(?P<id>\d+)/$', views.achievement_show, name='achievement_show'),
    url(r'^achievement/(?P<id>\d+)/accept$', views.achievement_accept, name='achievement_accept'),
    url(r'^achievement/(?P<id>\d+)/accomplish$', views.achievement_accomplish, name='achievement_accomplish'),
    # профиль 
    url(r'^user/(?P<steamid>\d+)/$', views.user_show, name='profile'),
    url(r'^user/update/$', views.user_update, name='profile_update'),
    # Детали матча
    url(r'^match/(?P<matchid>\d+)/$', views.match_show, name='match_details'),
    # Статические страницы
    url(r'^$', views.index, name='home'),
    # favicon
    url(r'^favicon\.ico$', RedirectView.as_view(url='/media/favicon.ico'))
)