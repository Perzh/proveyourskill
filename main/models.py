# coding: utf-8

from django.db import models
from django.contrib.auth.models import User, UserManager
from SteamAPI import *
import datetime
import re

class Achievement(models.Model):
    # Кол-во игр которые необходимо сыграть для достижения ачивки
    games_required = models.IntegerField()
    # Краткое название
    title = models.CharField(max_length=20)
    # Словесное описание ачивки
    description = models.TextField(max_length=1000)
    # Строка, описывающая параметры, необходимые для достижения ачивки
    player_stats = models.CharField(max_length=100, blank=True, null=True)
    match_stats = models.CharField(max_length=100, blank=True, null=True)

    def __unicode__(self):
        return self.title
    # Определяет выполнено ли задание
    def unlocked(self, usr=None):
        if usr is not None:
            # Запрашиваем последние games_required матчей сыгранных usr
            matches = GetMatchHistory({
                'account_id': usr.username,
                'matches_requested': self.games_required,
                'format': 'json'
            }).json()['result']['matches']
            for match in matches:
                # Поулчаем детали матча
                match_info = getMatchDetails({
                    'match_id': match['match_id'],
                    'format': 'json'
                }).json()['result']
                # Ищем инфу о usr в деталях
                for plr in match_info['players']:
                    # account_id - int32, username - строка, представляющая запись int64 steamid
                    if plr['account_id'] == SteamID32(usr.username):
                        player = plr
                        break
                # Строка player_stats имеет вид param1_(=,<,> )_value1&param2_(=,<,>)_value2
                # Сначала делим на условия param operator value с помощью апмерсанта
                if len(self.player_stats) > 0:
                    params = self.player_stats.split('&')
                    result = True
                    # Обходим и проверяем все условия на игрока
                    for condition in params:
                        condition = condition.split()
                        param = condition[0]
                        op = condition[1]
                        value = int(condition[2])

                        if op == '=':
                            result = result and (player[param] == value)
                        elif op == '<':
                            result = result and (player[param] < value)
                        elif op == '>':
                            result = result and (player[param] > value)
                        # Если условие не выполнено, дальнейшие условия нет смысла проверять и мы выходим из цикла
                        if not result:
                            break
                if not result:
                    break
                # Проверяем условия на матч
                if len(self.match_stats) > 0:
                    params = self.match_stats.split('&')
                    result = True
                    # Обходим и проверяем все условия на  матч
                    for condition in params:
                        condition = condition.split()
                        param = condition[0]
                        op = condition[1]
                        value = int(condition[2])

                        if op == '=':
                            result = result and (match_info[param] == value)
                        elif op == '<':
                            result = result and (match_info[param] < value)
                        elif op == '>':
                            result = result and (match_info[param] > value)
                        # Если условие не выполнено, дальнейшие условия нет смысла проверять и мы выходим из цикла
                        if not result:
                            break
                if not result:
                    break
            return result
        return False

class SteamUser(User):
    avatar_url = models.URLField(unique=True)
    profile_url = models.URLField(unique=True)
    personaname = models.CharField(max_length=100)
    realname = models.CharField(max_length=100)
    created_at = models.DateTimeField(null=True, blank=True)

    current_achievement = models.ForeignKey(Achievement, related_name='running_users', null=True)
    reached_achievements = models.ManyToManyField(Achievement, through='ReachedAchievement', related_name='done_users')

    objects = UserManager()

    def __unicode__(self):
        return unicode(self.username)

    def last_match_id(self):
        match = GetMatchHistory({
            'account_id': self.username,
            'matches_requested': 1,
            'format': 'json'
        }).json()['result']['matches']
        return match[0]['match_id']

    def fill_data(self):
        params = {'steamids': self.username, 'format': 'json'}
        info = GetPlayersSummaries(params)
        data = info.json()['response']['players'][0]
        self.avatar_url = data['avatar']
        self.personaname = data['personaname']
        self.profile_url = data['profileurl']
        try:
            self.created_at = datetime.date.fromtimestamp(data['timecreated'])
            self.realname = data['realname']
        except:
            pass

    def GetFullAvatarUrl(self):
        l = self.avatar_url.split('.jpg', 1)
        return l[0] + '_full.jpg'

    def GetMediumAvatarUrl(self):
        l = self.avatar_url.split('.jpg', 1)
        return l[0] + '_medium.jpg'

class Hero(models.Model):
    name = models.CharField(max_length=20, unique=True)
    imgURL = models.URLField(unique=True)

    def __unicode__(self):
        return self.name

    @staticmethod
    def FillTable():
        inf = GetHeroes({'language': 'en-us', 'format': 'json'}).json()
        heroes = inf['result']['heroes']
        for hero in heroes:
            try:
                # hero['name'] имеет вид npc_dota_hero_antimage
                # нам нужна тока последняя часть (antimage)
                url = 'http://cdn.dota2.com/apps/dota2/images/heroes/' + hero['name'][14:]
                Hero.objects.create(id=hero['id'], name=hero['localized_name'], imgURL=url)
            except Exception, e:
                print str(e)
        print "Done... %d objects written" % len(heroes)
    @property
    def GetImageURLsb(self):
        return self.imgURL + '_sb.png'

class Item(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __unicode__(self):
        return self.name

    def GetImageURLlg(self):
        return "http://cdn.dota2.com/apps/dota2/images/items/%s_lg.png" % self.name

class ReachedAchievement(models.Model):
    user = models.ForeignKey(SteamUser)
    achievement = models.ForeignKey(Achievement)
    match_id = models.IntegerField()
    reached_at = models.DateTimeField()

    def save(self):
        self.reached_at = datetime.datetime.now()
        super(ReachedAchievement, self).save()

class Lobby(models.Model):
    name = models.CharField(max_length=30)

class Mode(models.Model):
    name = models.CharField(max_length=100)