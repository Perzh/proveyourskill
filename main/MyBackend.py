# coding: utf-8

from django.conf import settings
from django.contrib.auth.backends import ModelBackend
from django.core.exceptions import ImproperlyConfigured
from django.db.models import get_model
from models import SteamUser

class SteamUserModelBackend(ModelBackend):
    def authenticate(self, username=None, password=None):
        try:
            user = SteamUser.objects.get(username=username)
            if user.check_password(password):
                return user
        except SteamUser.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return SteamUser.objects.get(pk=user_id)
        except SteamUser.DoesNotExist:
            return None