# coding: utf-8

import tempfile
from django.http import HttpResponseBadRequest, HttpResponseForbidden
from SteamAPI import *
from django.contrib import messages
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, redirect
# OPENID
from openid.consumer.consumer import Consumer, SUCCESS
from openid.store.filestore import FileOpenIDStore
#AUTH
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import AnonymousUser
from models import *
from django.conf import settings 

STORAGE = FileOpenIDStore(tempfile.gettempdir())

def oid_begin(request):
    try:
        consumer = Consumer(request.session, STORAGE)
        oid_request = consumer.begin(settings.PROVIDER)
        url = oid_request.redirectURL(settings.ROOT, settings.RETURN_TO)
        return redirect(url)
    except Exception, e:
        return HttpResponseBadRequest(unicode(e))

def oid_complete(request):
    # МАГИЯ
    consumer = Consumer(request.session, STORAGE)
    inf = consumer.complete(request.REQUEST, settings.RETURN_TO)

    # Если подверждение личности не было успешным, выдаем ошибку
    if inf.status is not SUCCESS:
        return HttpResponseForbidden(u'Ошибка авторизации. Попробуйте позже.')

    # Получаем steamid пользователя из claimed_id, полученный от Steam-сервера
    # http:://ifnbi/id/steaim
    steamid = inf.identity_url.split('/')[5]

    user = authenticate(username=steamid, password=steamid)

    # Если пользователь с таким steamid уже есть в базе, то логиним
    if user is not None:
        login(request, user)
    else:
        # иначе делаем запрос по SteamAPI, чтобы получить инфу
        # затем заполняем и сохраняем пользователя в базе

        # Создаем юзера с полученным steamid
        user = SteamUser()

        user.username = steamid
        user.set_password(steamid)

        # fill_data инкапсулируем обращение к Steam серверу за инфой
        user.fill_data()
        user.save()

        # Аутентификация по steamid (он вместо username и password)
        user = authenticate(username=steamid, password=steamid)

        login(request, user)

    return redirect('/user/%s' % steamid)

def signout(request):
    logout(request)
    return redirect('/')

def user_show(request, steamid):
    try:
        user = SteamUser.objects.get(username=steamid)

        flag = request.user is not AnonymousUser and request.user.username == user.username

        # Берем только первые пять матчей в json-формате
        params = {
            'matches_requested': 5, 
            'account_id': steamid, 
            'format': 'json'
        }
        last_matches_request = GetMatchHistory(params)
        match_data = []
        error = ''
        # Если получен успешные ответ
        if last_matches_request.json()['result']['status'] == 1:
            matches = last_matches_request.json()['result']['matches']
            # Магия: id указанные в инфе о матче это 32битные варианты steamid пользователей
            acc_id = SteamID32(user.username)

            for match in matches:
                players = match['players']
                for player in players:
                    if player['account_id'] == acc_id:
                        h = Hero.objects.get(pk=player['hero_id'])
                        match_data.append({ 'hero': h, 'matchid': match['match_id'] })
        else:
            error = '''You need to enable the "Share Match History" 
            setting in the Dota 2 client in order to have your statistics 
            appear on ProveYourSkills.'''
        # Используем стандартную пагинацию для вывода ачивок в профиль
        paginator = Paginator(
            # Скраиваю в список все достижения и связи достижений и пользователя
            # Связь хранит id матча, в котором было выполнено достижение
        	zip(user.reached_achievements.all(), ReachedAchievement.objects.filter(user=user)), 
        	10
        )
        # получаем номер странцы
        page = request.GET.get('page')
        try:
            pagen_achievements_info = paginator.page(page)
        except PageNotAnInteger:
            # Если страница не целое число, то выводи первую страницу
            pagen_achievements_info = paginator.page(1)
        except EmptyPage:
            # Если страница за допустимыми пределами, то выводим последнюю страницу
            pagen_achievements_info = paginator.page(paginator.num_pages)

        return render(request, 'main/user_show.html', {
            'error': error,
            'match_data': match_data,
            'usr': user,
            'logged_in': flag,
            'current_achievement': user.current_achievement,
            'achievements_info': pagen_achievements_info
        })
    except SteamUser.DoesNotExist:
        return HttpResponseBadRequest('This Steam user is not registered on ProveYourSkills')
    except KeyError:
        return render(request, 'main/user_show.html', {
            'error': 'Steam is currently unavailable',
            'usr': user,
            'logged_in': flag,
        })

def user_update(request):
    user = request.user

    if user.is_authenticated():
        user.fill_data()
        user.save()
        return redirect('/user/%s' % user.username)
    else:
        return HttpResponseForbidden(u'Для доступа к профилю необходимо авторизоваться в Steam')

def match_show(request, matchid):
    details = getMatchDetails({'match_id': matchid, 'format': 'json'}).json()

    # Вычисляем продолжительность
    duration = int(details['result']['duration'])
    hours =  int(duration / 3600)
    minutes = int(duration / 60) - 60 * hours
    seconds = duration - minutes * 60

    # Добавляем объекты Hero по hero_id, чтобы выводить иконки
    for player in details['result']['players']:
        player['hero'] = Hero.objects.get(pk=player['hero_id'])
        inf = GetPlayersSummaries({
            'format': 'json',
            'steamids': SteamID64(player['account_id']),
        }).json()['response']['players']
        # если юзер срыл профиль, то список players будет пуст
        # иначе он будет содержать словарь с инфой
        if len(inf) > 0:
            player['personaname'] = inf[0]['personaname']
            player['profile'] = inf[0]['profileurl']
            player['avatar'] = inf[0]['avatar']
        else:
            player['personaname'] = 'Anonymous'
            player['profile'] = ''
            player['avatar'] = ''

    # Игроки
    players = {'radiant': [], 'dire': []}
    for i in range(0, 5):
        players['radiant'].append(details['result']['players'][i])

    for i in range(5, 10):
        players['dire'].append(details['result']['players'][i])

    lobby = Lobby.objects.get(pk=details['result']['lobby_type'])
    details['result']['lobby_type'] = lobby.name

    mode = Mode.objects.get(pk=details['result']['game_mode'])
    details['result']['game_mode'] = mode.name

    context = {
        'matchid': matchid,
        'details': details['result'],
        'duration': {
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        },
        'players': players
    }
    return render(request, 'main/match_show.html', context)

def achievement_all(request):
    paginator = Paginator(Achievement.objects.all(), 15)
    page = request.GET.get('page')
    try:
        achievements = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        achievements = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        achievements = paginator.page(paginator.num_pages)
    return render(request, 'main/achievement_all.html', { 'achievements': achievements })

def achievement_show(request, id):
    try:
        achievement = Achievement.objects.get(pk=id)
        return render(request, 'main/achievement_show.html', {
            'achievement': achievement
        })
    except Achievement.DoesNotExist:
        return HttpResponseBadRequest("Achievement doesn't exist")

def achievement_accept(request, id):
    try:
        achievement = Achievement.objects.get(pk=id)
        user = request.user
        if user.is_authenticated():
            if not achievement in user.reached_achievements.all():
                user.current_achievement = achievement
                user.save()
                messages.success(request, "Challenge accepted!")
                return redirect('profile', user)
            else:
                messages.error(request, "Can't accept challenge already accomplished")
                return redirect('achievement_show', achievement.id)
        else:
            return redirect('login')
    except Achievement.DoesNotExist:
        return HttpResponseBadRequest("Achievement doesn't exist")

def achievement_accomplish(request, id):
    try:
        achievement = Achievement.objects.get(pk=id)
        user = request.user
        if user.is_authenticated():
            if achievement.unlocked(user):
                user.current_achievement = None
                r = ReachedAchievement(user=user, achievement=achievement, match_id=int(user.last_match_id()))
                r.save()
                user.save()
                messages.success(request, "Achievement unlocked!")
            else:
                messages.warning(request, "Can't accomplish challenge... =(")
            return redirect('profile', user)
        else:
            return redirect('login')
    except Achievement.DoesNotExist:
        return HttpResponseBadRequest("Achievement doesn't exist")

# STATIC PAGES
def index(request):
    return render(request, 'main/index.html')

def about(request):
    pass

def privacy(request):
    pass