# coding: utf-8
import requests
from django.conf import settings

def AddGetParams(url, params):
    for key in params.keys():
        url = url + ("&%s=%s" % (key, params[key]))

    return url

def GetPlayersSummaries(params):
    # Адрес запроса к SteamAPI за инфой о пользователе по steamid
    url = "https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=%s" % settings.STEAM_API_KEY
    # Добавляем параметры
    url = AddGetParams(url, params)
    # Делаем запрос
    return requests.get(url)

# Получить историю матчей (http://dev.dota2.com/showthread.php?t=58317)
def GetMatchHistory(params):
    # Адрес запроса к SteamAPI
    url = "https://api.steampowered.com/IDOTA2Match_570/GetMatchHistory/v001/?key=%s" % settings.STEAM_API_KEY
    # Добавляем параметры
    url = AddGetParams(url, params)
    # Делаем запрос
    return requests.get(url)

def GetHeroes(params):
    # Адрес запроса к SteamAPI
    url = "https://api.steampowered.com/IEconDOTA2_570/GetHeroes/v001/?key=%s" % settings.STEAM_API_KEY
    # Добавляем параметры
    url = AddGetParams(url, params)
    # Делаем запрос
    return requests.get(url)

def getMatchDetails(params):
    # Адрес запроса к SteamAPI
    url = "https://api.steampowered.com/IDOTA2Match_570/GetMatchDetails/V001/?key=%s" % settings.STEAM_API_KEY
    # Добавляем параметры
    url = AddGetParams(url, params)
    # Делаем запрос
    return requests.get(url)

def int32(x):
    # Магическая константа - маска, которая содержит
    # первые 32бита нулей, затем 32бита единичек
    # усекает значение long до int32(как бы)
    return x & 4294967295

def SteamID64(x):
    return int(x) | 76561197960265728;

def SteamID32(x):
    return int(x) & 4294967295;