from django.contrib import admin
from main.models import *

# Register your models here.
admin.site.register(SteamUser)
admin.site.register(Achievement)
admin.site.register(Hero)
admin.site.register(Item)